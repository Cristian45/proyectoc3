import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";

axios.defaults.baseURL = "https://travelguest.herokuapp.com/";
// import Vue3Material from "vue3-material";

createApp(App)
  .use(router)
  .use(store)
  //   .use(Vue3Material)
  .mount("#app");
