import { createRouter, createWebHashHistory } from "vue-router";
import store from "../store";
import Home from "../views/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/hotels",
    name: "Hotels",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Hotels.vue"),
  },
  {
    path: "/hotels/:id",
    name: "Hotel",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Hotel.vue"),
  },
  {
    path: "/contact",
    name: "Contact",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Contact.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Login.vue"),
  },
  {
    path: "/register",
    name: "Register",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Register.vue"),
  },
  {
    path: "/profile",
    name: "Profile",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Profile.vue"),
  },
  // {
  //   path: "/list",
  //   name: "HotelList",
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/HotelList.vue"),
  // },

  {
    path: "/booking-user",
    name: "UserBooking",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/UserBooking.vue"),
    beforeEnter(to, from, next) {
      // check vuex store //
      if (!store.state.isAuthenticated) {
        next({
          name: "Login", // back to safety route //
          query: { redirectFrom: to.fullPath },
        });
      } else {
        next();
      }
    },
  },
  {
    path: "/booking-form/:id",
    name: "FormBooking",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/FormBooking.vue"),
    beforeEnter(to, from, next) {
      // check vuex store //
      if (!store.state.isAuthenticated) {
        next({
          name: "Login", // back to safety route //
          query: { redirectFrom: to.fullPath },
        });
      } else {
        next();
      }
    },
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.fullPath === "/contact") {
    if (!store.state.isAuthenticated) {
      next("/login");
    }
  }
  if (to.fullPath === "/login" || to.fullPath === "/register") {
    if (store.state.isAuthenticated) {
      next("/contact");
    }
  }
  next();
});

export default router;
