import { createStore } from "vuex";
import {
  getUserData,
  handleAuthentication,
  saveAuthStorageHandler,
  logoutStorageHandler,
  saveUserDataStorageHandler,
} from "../utils/authenticate";

import axios from "axios";

axios.defaults.baseURL = "https://travelguest.herokuapp.com/";

const userData = getUserData();

const store = createStore({
  state: {
    count: 0,
    ...handleAuthentication(userData),
    authError: false,
    hotels: [],
  },
  getters: {
    getHotelById: (state) => (id) => {
      return state.hotels.filter((hotel) => hotel.id === id);
    },
  },
  mutations: {
    increment(state) {
      state.count++;
    },
    incrementBy(state, payload) {
      state.count += payload;
    },
    logoutSuccess: (state) => {
      state.user = null;
      state.isAuthenticated = null;
      state.token = null;
      state.expiry = null;
      logoutStorageHandler();
    },
    saveAuthData: (state, payload) => {
      const { user, expiry, token } = payload;
      saveAuthStorageHandler(payload);
      state.user = user;
      state.isAuthenticated = true;
      state.expiry = expiry;
      state.token = token;
    },
    setAuthError: (state) => {
      state.authError = true;
      state.isAuthenticated = false;
    },
    updateUserSuccess: (state, payload) => {
      state.userData = payload;
      saveUserDataStorageHandler(payload);
    },
    loadHotelsSuccess: (state, payload) => {
      state.hotels = payload;
    },
  },
  actions: {
    logInUser(context, payload) {
      return axios
        .post("/api/login/", payload)
        .then((response) => {
          context.commit("saveAuthData", response.data);
        })
        .catch((err) => {
          if (err.response.status === 400) {
            context.commit("setAuthError");
          }
        });
    },
    loadHotels(context) {
      let url = "/api/hotel/";
      return axios
        .get(url)
        .then((res) => {
          context.commit("loadHotelsSuccess", res.data);
        })
        .catch((err) => console.log(err));
    },
    register(context, payload) {
      return axios
        .post("/api/register/", payload)
        .then((response) => {
          context.commit("saveAuthData", response.data);
        })
        .catch((err) => {
          if (err.response.status) {
            context.commit("setAuthError");
          }
        });
    },
    logout(context) {
      const headers = {
        Authorization: `Token ${context.state.token}`,
      };
      return axios
        .post("/api/logout/", null, { headers })
        .then((response) => {
          context.commit("logoutSuccess");
        })
        .catch((err) => {
          if (err.response.status) {
            console.log(err);
            context.commit("setAuthError");
          }
        });
    },
    updateUserProfile(context) {
      const headers = {
        Authorization: `Token ${context.state.token}`,
      };
      const data = context.state.userData;
      return axios
        .put(`/api/update-user/${data.id}/`, data, { headers })
        .then((response) => {
          context.commit("updateUserSuccess", response.data);
        })
        .catch((err) => {
          if (err.response.status) {
            context.commit("setAuthError");
          }
        });
    },
  },
});

export default store;
